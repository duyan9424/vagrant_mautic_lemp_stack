# Vagrant Mautic 2.11.0 - LEMP Stack PHP 7.0

A simple Vagrant Mautic 2.11.0 - LEMP setup running PHP7.

## Requirements

To get up and running with this environment, you first need to have Virtualbox and Vagrant installed on your system.

* [Virtualbox Downloads](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant Downloads](https://www.vagrantup.com/downloads.html)

## What's inside?

- Official Ubuntu 16.04 x64 LTS (Xenial Xerus)
- Nginx
- PHP7 with some extensions
- MariaDB
- Composer
- Mautic version 2.11.0

## How to use

- Clone this repository into your project or You can download the `Vagrantfile` from this repo.
- Run ``vagrant up``
- Navigate to ``http://192.168.33.34/`` 

## Database

- For MariaDB the default user is root with password 'root': sudo mysql -u root -p

## Recommended Workflow
- You can run `vagrant ssh`. This will shell you in to your local Vagrant instance.
- Mount shared folder: /var/www/project => ./project

## Virtual Machine Management
- to shutdown: `vagrant halt`
- to boot it again: `vagrant up`
- You can find out the state of a virtual machine anytime: `vagrant status`
- Please check the [Vagrant documentation](https://www.vagrantup.com/docs/cli/) for more information on Vagrant.