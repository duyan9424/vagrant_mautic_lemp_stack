$script = <<SCRIPT
#!/usr/bin/env bash

echo "Provisioning virtual machine..."

echo "Find Out Linux Distribution Name and Release Version"
cat /etc/lsb-release

echo "Updating apt-get..."
sudo apt-get update > /dev/null

# Nginx
echo "Installing Nginx..."
sudo apt-get install -y nginx > /dev/null

# PHP
echo "Updating PHP repository..."
sudo apt-get install -y python-software-properties build-essential > /dev/null
sudo add-apt-repository -y ppa:ondrej/php > /dev/null

echo "Updating apt-get once more..."
sudo apt-get update > /dev/null

echo "Installing PHP..."
sudo apt-get install -y php7.0 php7.0-fpm > /dev/null

echo "Installing PHP extensions..."
sudo apt-get install -y curl php7.0-common php7.0-cli php7.0-mysql php7.0-simplexml php7.0-curl php7.0-zip > /dev/null
sudo apt-get install -y curl php7.0-xml php7.0-mcrypt php7.0-imap php7.0-mailparse php7.0-mbstring php7.0-bcmath> /dev/null

# MySQL
echo "Preparing Mariadb..."
sudo apt-get install -y debconf-utils > /dev/null
debconf-set-selections <<< "mariadb-server mariadb-server/root_password password root"
debconf-set-selections <<< "mariadb-server mariadb-server/root_password_again password root"

echo "Installing Mariadb..."
sudo apt-get install -y mariadb-server mariadb-client > /dev/null

echo "Having MariaDB installed, you can confirm the installation with:"
mysql -V

# Nginx Config
echo "Configuring Nginx..."

sudo rm /etc/nginx/sites-available/default
sudo touch /etc/nginx/sites-available/default

cat << 'EOF' > /tmp/default
server {
  listen 80;
  root /var/www/project/mautic/;
  index index_dev.php index.html index.htm;

  # Important for VirtualBox
  sendfile off;

  location / {
	try_files $uri $uri/ =404;
  }

  location ~* \.php {
	include fastcgi_params;

	fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;

	fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
	fastcgi_cache off;
	fastcgi_index index.php;
  }
}
EOF
# nginx_vhost
sudo mv /tmp/default /etc/nginx/sites-available/default

echo "Install Composer..."
curl -s https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
sudo chmod +x /usr/local/bin/composer

echo "Clone the repository Git Mautic..."
# If the current directory is empty.
cd /var/www/project/
git clone -b 2.11.0 --single-branch https://github.com/mautic/mautic.git . > /dev/null

cd /var/www/project/mautic/
echo "Composer install..."
composer install > /dev/null

echo "Change the mode 777, and Owner..."
# Change the owner of `mautic` directory to `www-data`. Verify Ngnix-User from `/etc/nginx/nginx.conf`
sudo chown -R www-data:www-data /var/www/project/mautic/ > /dev/null
   
cd /var/www/project/mautic/
sudo chmod -R 755 app media translations > /dev/null

# Restarting Nginx for config to take effect
echo "Restarting Nginx..."
sudo systemctl restart nginx > /dev/null
sudo systemctl restart php7.0-fpm > /dev/null

echo "+---------------------------------------------------------+"
echo "|                      S U C C E S S                      |"
echo "+---------------------------------------------------------+"
echo "|   You're good to go! You can now view your server at    |"
echo "|           \"192.168.33.34/\" in a browser.              |"
echo "|                                                         |"
echo "|                                                         |"
echo "|          You can SSH in with ubuntu / vagrant           |"
echo "|                                                         |"
echo "|        You can login to MySQL with root / root          |"
echo "+---------------------------------------------------------+"
SCRIPT

# Fix the warning: dpkg-reconfigure: unable to re-open stdin: No file or directory
# export DEBIAN_FRONTEND=noninteractive

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "ubuntu/xenial64"
  config.vm.network "private_network", ip: "192.168.33.34"

  # config.vm.network :forwarded_port, guest: 80, host: 8080, auto_correct: true

  config.vm.synced_folder "./", "/vagrant", disabled: true
  config.vm.synced_folder "./project/mautic", "/var/www/project/mautic", create: true, group: "www-data", owner: "www-data"

  config.vm.provider :virtualbox do |vb|
    vb.name = "Vagrant Mautic LEMP Stack"
    vb.customize ["modifyvm", :id, "--memory", "1024"]
  end
  
  config.vm.provision "shell", inline: $script
end
